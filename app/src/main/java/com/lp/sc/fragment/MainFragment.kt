package com.lp.sc.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lp.sc.R
import com.lp.sc.twitter.TwitterService

class MainFragment : Fragment(), TwitterService.TokenFetchedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("mainfragment", "Fragment created")

        val twitter = TwitterService(this)
        twitter.fetchToken()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("mainfragment", "Fragment view created")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onTokenFetched(tok: String) {
        requireActivity().getSharedPreferences("SC_PREFS", Context.MODE_PRIVATE)
            .edit().putString("TWITTER_TOKEN", tok).apply()
    }
}