package com.lp.sc.twitter.models

import com.google.gson.annotations.SerializedName

data class Entity(
    val hashtags: List<Hashtag>?,
    val media: List<Media>?,
    val urls: List<Url>?,
    @SerializedName("user_mentions") val userMentions: List<UserMention>?,
    val symbols: List<Symbols>?,
    val polls: List<Poll>?
)