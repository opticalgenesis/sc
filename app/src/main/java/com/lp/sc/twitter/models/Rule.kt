package com.lp.sc.twitter.models

import com.google.gson.annotations.SerializedName

data class Rule(
    val tag: String,
    val id: Long,
    @SerializedName("id_str") val idStr: String
)