package com.lp.sc.twitter.models

data class Hashtag(
    val indices: List<Int>,
    val text: String
)