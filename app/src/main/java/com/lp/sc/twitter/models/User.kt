package com.lp.sc.twitter.models

import com.google.gson.annotations.SerializedName

data class User(
    val id: Long,
    @SerializedName("id_str") val idStr: String,
    val name: String,
    @SerializedName("screen_name") val screenName: String,
    val location: String?,
    val derived: Any?,
    val url: String?,
    val description: String?,
    val protected: Boolean,
    val verified: Boolean,
    @SerializedName("followers_count") val followersCount: Int,
    @SerializedName("friends_count") val friendsCount: Int,
    @SerializedName("listed_count") val listedCount: Int,
    @SerializedName("favourites_count") val favCount: Int,
    @SerializedName("statuses_count") val statusesCount: Int,
    @SerializedName("created_at") val createdAt: String,
    @SerializedName("profile_banner_url") val profileBannerUrl: String,
    @SerializedName("profile_image_url_https") val secureProfileBannerUrl: String,
    @SerializedName("default_profile") val defaultProfile: Boolean,
    @SerializedName("default_profile_image") val defaultProfileImage: Boolean,
    @SerializedName("withheld_in_countries") val withheldInCountries: List<String>,
    @SerializedName("withheld_scope") val withheldScope: String)