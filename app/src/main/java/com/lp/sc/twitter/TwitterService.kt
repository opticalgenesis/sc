package com.lp.sc.twitter

import android.util.Log
import com.lp.sc.keys.Keys
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TwitterService(private val li: TokenFetchedListener) {

    fun fetchToken() {
        val client = OkHttpClient.Builder()
            .addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                    val req = chain.request().newBuilder()
                        .addHeader(
                            "Authorization", Credentials.basic(Keys.ACCESS_TOKEN, Keys.ACCESS_TOKEN_SECRET)
                        ).build()

                    return chain.proceed(req)
                }
            })
        val r = Retrofit.Builder()
            .client(client.build())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.twitter.com")
            .build().create(TwitterApi::class.java)

        val a = r.getBearerToken("client_credentials")
        a.enqueue(object : Callback<TwitterJwtResponse> {
            override fun onResponse(
                call: Call<TwitterJwtResponse>,
                response: Response<TwitterJwtResponse>
            ) {
                if (response.isSuccessful) {
                    if (response.body() == null) {
                        Log.e("OAUTH", "Body is null")
                    } else {
                        li.onTokenFetched(response.body()?.accessToken!!)
                    }
                } else {
                    Log.d("OAUTH", "Failure: ${response.raw()}")
                }
            }

            override fun onFailure(call: Call<TwitterJwtResponse>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    interface TokenFetchedListener {
        fun onTokenFetched(tok: String)
    }
}