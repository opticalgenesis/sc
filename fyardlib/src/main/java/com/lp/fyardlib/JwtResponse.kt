package com.lp.fyardlib

data class JwtResponse(val token: String)