package com.lp.fyardlib

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageModelGallery(
    val id: String,
    val name: String,
    val slug: String
): Parcelable